package formation.year2122.campusacademy.java.crm.repository;

import formation.year2122.campusacademy.java.crm.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
