package formation.year2122.campusacademy.java.crm.service;

import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    public List<Order> getAll() {
        return this.orderRepository.findAll();
    }

    public void delete(Long id) throws EntityNotFoundException {
        Optional<Order> optionalOrder = this.orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            this.orderRepository.delete(optionalOrder.get());
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Optional<Order> findById(Long id) {
        return this.orderRepository.findById(id);
    }

    public Order save(Order order) {
        return this.orderRepository.save(order);
    }

    public Order create(Order order) {
        order.setId(null);
        return this.save(order);
    }
}
