package formation.year2122.campusacademy.java.crm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "type_prestation")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TypePrestation {
    @Id
    String name;
    String description;

}
