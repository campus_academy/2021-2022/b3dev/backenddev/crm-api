package formation.year2122.campusacademy.java.crm.api.dto;

import formation.year2122.campusacademy.java.crm.model.OrderStateEnum;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class OrderDTO extends OrderMinimalDTO {
    private int tjmHt;
    private int nbJours;
    private int tva;
    private OrderStateEnum state;
    private ClientMinimalDTO client;
    private String comment;
}
