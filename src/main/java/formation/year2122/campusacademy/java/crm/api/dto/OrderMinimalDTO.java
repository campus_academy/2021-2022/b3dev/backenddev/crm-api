package formation.year2122.campusacademy.java.crm.api.dto;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@Data
@SuperBuilder
public class OrderMinimalDTO {
    private Long id;
    private String typePresta;
}
