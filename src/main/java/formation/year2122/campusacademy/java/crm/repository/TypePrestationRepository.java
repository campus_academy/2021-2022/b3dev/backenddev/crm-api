package formation.year2122.campusacademy.java.crm.repository;

import formation.year2122.campusacademy.java.crm.model.TypePrestation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "typePrestations", path = "typePrestations")
public interface TypePrestationRepository extends JpaRepository<TypePrestation, String> {
}
