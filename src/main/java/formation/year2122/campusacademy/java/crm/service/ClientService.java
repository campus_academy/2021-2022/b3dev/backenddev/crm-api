package formation.year2122.campusacademy.java.crm.service;

import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public List<Client> getAll() {
        return this.clientRepository.findAll();
    }

    public void delete(Long id) throws EntityNotFoundException {
        Optional<Client> optionalClient = this.clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            this.clientRepository.delete(optionalClient.get());
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Optional<Client> findById(Long id) {
        return this.clientRepository.findById(id);
    }

    public Optional<Client> findByIdAndName(Long id, String name) {
        return this.clientRepository.findClientByIdAndName(id, name);
    }

    public Client save(Client client) {
        return this.clientRepository.save(client);
    }

    public Client create(Client client) {
        client.setId(null);
        return this.save(client);
    }

    public List<Client> searchByName(String name, Long page, Long size) {
        return this.clientRepository.findClientByNameStartsWith(
                name,
                PageRequest.of(page.intValue(), size.intValue())
        );
    }
}
