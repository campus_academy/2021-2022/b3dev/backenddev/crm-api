package formation.year2122.campusacademy.java.crm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    private Long turnover = 0L;
    @Enumerated(EnumType.STRING)
    private ClientStateEnum state = ClientStateEnum.INACTIF;
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private List<Order> orders = Collections.emptyList();

}
