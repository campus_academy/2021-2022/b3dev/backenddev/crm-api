package formation.year2122.campusacademy.java.crm.api;

import formation.year2122.campusacademy.java.crm.api.dto.ClientDTO;
import formation.year2122.campusacademy.java.crm.api.dto.ClientMinimalDTO;
import formation.year2122.campusacademy.java.crm.api.dto.ClientWithoutOrdersDTO;
import formation.year2122.campusacademy.java.crm.api.dto.OrderDTO;
import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.service.ClientService;
import formation.year2122.campusacademy.java.crm.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping(path = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final ClientService clientService;

//    public OrderController(OrderService orderService) {
//        this.orderService = orderService;
//    }

    @GetMapping(path = "{id}")
    public ResponseEntity<OrderDTO> getById(@PathVariable Long id) {
        return this.orderService.findById(id)
                .map(order -> ResponseEntity.ok(mapToDTO(order)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<OrderDTO>> getAll() {
        return ResponseEntity.ok(
                mapToDTOs(this.orderService.getAll())
        );
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(
            @RequestBody OrderDTO orderDTO
    ) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.orderService.create(mapToEntity(orderDTO))));
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> update(
            @PathVariable Long id,
            @RequestBody OrderDTO orderDTO
    ) {
        if (this.orderService.findById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        if (!Objects.equals(id, orderDTO.getId())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(mapToDTO(this.orderService.save(mapToEntity(orderDTO))));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            this.orderService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private Order mapToEntity(OrderDTO orderDTO) {

        Client client = this.clientService
                .findByIdAndName(
                        orderDTO.getClient().getId(),
                        orderDTO.getClient().getName()
                )
                .orElseThrow();

        Order order = new Order();
        order.setId(orderDTO.getId());
        order.setTjmHt(orderDTO.getTjmHt());
        order.setNbJours(orderDTO.getNbJours());
        order.setTva(orderDTO.getTva());
        order.setState(orderDTO.getState());
        order.setTypePresta(orderDTO.getTypePresta());
        order.setClient(client);
        order.setComment(orderDTO.getComment());
        order.setState(orderDTO.getState());
        return order;
    }

    List<OrderDTO> mapToDTOs(List<Order> orders) {
        return orders.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    private OrderDTO mapToDTO(Order order) {
        return OrderDTO.builder()
                .id(order.getId())
                .tjmHt(order.getTjmHt())
                .nbJours(order.getNbJours())
                .tva(order.getTva())
                .state(order.getState())
                .typePresta(order.getTypePresta())
                .client(mapToMinimalDTO(order.getClient()))
                .comment(order.getComment())
                .state(order.getState())
                .build();
    }

    private ClientMinimalDTO mapToMinimalDTO(Client client) {
        return ClientMinimalDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .build();
    }

}
