package formation.year2122.campusacademy.java.crm.api;

import formation.year2122.campusacademy.java.crm.api.dto.*;
import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.awt.print.Pageable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping(path = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

//    public ClientController(ClientService clientService) {
//        this.clientService = clientService;
//    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ClientDTO> getById(@PathVariable Long id) {
        return this.clientService.findById(id)
                .map(client -> ResponseEntity.ok(mapToDTO(client)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<ClientDTO>> getAll() {
        return ResponseEntity.ok(
                mapToDTOs(this.clientService.getAll())
        );
    }


    @GetMapping(path = "search")
    public ResponseEntity<List<ClientDTO>> search(
            @RequestParam String name,
            @RequestParam(defaultValue = "0", required = false) Long page,
            @RequestParam(defaultValue = "10", required = false) Long size
    ) {
        return ResponseEntity.ok(
                mapToDTOs(this.clientService.searchByName(name, page, size))
        );
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientWithoutOrdersDTO> create(
            @RequestBody ClientWithoutOrdersDTO clientDTO
    ) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToWithoutOrdersDTO(this.clientService.create(mapToEntity(clientDTO))));
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientWithoutOrdersDTO> update(
            @PathVariable Long id,
            @RequestBody ClientWithoutOrdersDTO clientDTO
    ) {
        if (this.clientService.findById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        if (!Objects.equals(id, clientDTO.getId())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(mapToDTO(this.clientService.save(mapToEntity(clientDTO))));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            this.clientService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    // For post/put no need of orders
    private Client mapToEntity(ClientWithoutOrdersDTO clientDTO) {
        Client client = new Client();
        client.setId(clientDTO.getId());
        client.setName(clientDTO.getName());
        client.setTurnover(clientDTO.getCa());
        client.setState(clientDTO.getState());
        return client;
    }

    private ClientWithoutOrdersDTO mapToWithoutOrdersDTO(Client client) {
        return ClientWithoutOrdersDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .ca(client.getTurnover())
                .state(client.getState())
                .build();
    }

    List<ClientDTO> mapToDTOs(List<Client> clients) {
        return clients.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    private ClientDTO mapToDTO(Client client) {
        return ClientDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .ca(client.getTurnover())
                .state(client.getState())
                .orders(mapToMinimalDTOs(client.getOrders()))
                .build();
    }

    List<OrderMinimalDTO> mapToMinimalDTOs(List<Order> clients) {
        return clients.stream()
                .map(this::mapToMinimalDTO)
                .collect(Collectors.toList());
    }

    private OrderMinimalDTO mapToMinimalDTO(Order order) {
        return OrderMinimalDTO.builder()
                .id(order.getId())
                .typePresta(order.getTypePresta())
                .build();
    }

}
