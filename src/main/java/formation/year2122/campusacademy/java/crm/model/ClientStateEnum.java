package formation.year2122.campusacademy.java.crm.model;

public enum ClientStateEnum {
    ACTIF, INACTIF, ERROR;
}
