package formation.year2122.campusacademy.java.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;

@SpringBootApplication

public class CrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmApplication.class, args);
	}

	@Bean
	public RepositoryDetectionStrategy.RepositoryDetectionStrategies config() {
		return RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED;
	}
}
