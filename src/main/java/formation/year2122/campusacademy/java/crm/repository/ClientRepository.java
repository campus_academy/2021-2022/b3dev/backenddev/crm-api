package formation.year2122.campusacademy.java.crm.repository;

import formation.year2122.campusacademy.java.crm.model.Client;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
//    @Query("select c from Client c where c.id = ?1 and c.name = ?2")
    Optional<Client> findClientByIdAndName(Long id, String name);

    @Query("select c from Client c where c.name like concat(?1, '%')")
    List<Client> findClientByNameStartsWith(String name, PageRequest pageRequest);
}
