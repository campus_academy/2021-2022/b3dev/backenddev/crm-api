package formation.year2122.campusacademy.java.crm.api.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Jacksonized
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class ClientDTO extends ClientWithoutOrdersDTO{
    private List<OrderMinimalDTO> orders;
}
