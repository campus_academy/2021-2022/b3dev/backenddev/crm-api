package formation.year2122.campusacademy.java.crm.api.dto;

import formation.year2122.campusacademy.java.crm.model.ClientStateEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;


@Jacksonized
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class ClientWithoutOrdersDTO extends ClientMinimalDTO {
    private Long ca;
    private ClientStateEnum state;
}

