package formation.year2122.campusacademy.java.crm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "`order`")
//@Table(name = "\"order\"")
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private int tjmHt = 0;
    @Column(nullable = false)
    private int nbJours = 0;
    @Column(nullable = false)
    private int tva = 0;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OrderStateEnum state = OrderStateEnum.OPTION;
    @Column(nullable = false)
    private String typePresta;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Client client;
    private String comment;
}
